# so we can test with browserstack
FROM nginx:latest

# Remove the default Nginx configuration file
RUN rm -v /etc/nginx/nginx.conf

# Copy a configuration file from the current directory
COPY conf/nginx.conf /etc/nginx/

COPY src/* /usr/share/nginx/html/
COPY src/* /var/www/html/

# Append "daemon off;" to the beginning of the configuration
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Expose ports
EXPOSE 90

# Set the default command to execute
# when creating a new container
CMD service nginx start


## build: docker build -t imagemakers .

## run: docker run -v $(pwd)/html/:/usr/share/nginx/html -p 8080:90 --name img imagemakers

## docker exec -it img bash
