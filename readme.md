# View instructions

## Direct file access
All html, css, images, and js are located in the html folder and can be opened directly in the browser.

## Docker
To help with testing across devices on the same network you can build and run the project as a docker image

See lines 24 - 26 of the Dockerfile for commands to build and run the project
